import xml.etree.ElementTree as ET

from common import *


@dataclass
class CopyTask:
    src_dir: Path
    dst_dir: Path
    filename: str

    def __repr__(self):
        return f'\'{self.filename}\' (\'{self.src_dir}\' -> \'{self.dst_dir}\')'

    async def execute(self):
        '''
      Copy the file asynchronously.
    '''
        src = self.src_dir / self.filename
        dst = self.dst_dir / self.filename
        assert src.exists(), "source file doesn't exist"
        if not self.dst_dir.exists():
            self.dst_dir.mkdir(mode=0o777, parents=True)
        assert self.dst_dir.exists(), "should be impossible"
        b = await read_file(src, 'rb')
        await write_file(dst, b, 'wb')


class CopyConfig:
    def __init__(self):
        super().__init__()
        self.entries = []

    @staticmethod
    def from_xml(xml: str) -> 'CopyConfig':
        '''
      Parse a XML string into a config.
    '''
        tree = ET.fromstring(xml)
        assert tree.tag == 'config'
        ret = CopyConfig()
        for c in tree:
            assert c.tag == 'file'
            src = c.attrib['source_path']
            dst = c.attrib['destination_path']
            src, dst = (Path(x) for x in (src, dst))
            filename = c.attrib['file_name']
            ret.entries.append(CopyTask(src, dst, filename))
        return ret

    @staticmethod
    async def read_file(file: Union[Path, str]) -> 'CopyConfig':
        '''
      Asynchronously load an XML file and parse it.
    '''
        res = CopyConfig.from_xml(await read_file(file, 'r'))
        return res

    async def execute_all(self):
        '''
      Run all copy tasks asynchronously.
    '''
        await execute_all((task.execute() for task in self.entries),
                          self.entries)
