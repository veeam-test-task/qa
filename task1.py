from sys import argv, exit

from common import *
from copy_config import CopyConfig


async def main_async():
    # one *should* use argparse in such cases, but this one is trivially a single parameter, so why bother
    if not len(argv) >= 2:
        print('usage: python3 task1.py [config_file]')
        exit(1)
    fn = argv[1]
    copy = await CopyConfig.read_file(fn)
    await copy.execute_all()


if __name__ == '__main__':
    aio.run(main_async())
