from sys import argv
import hashlib

from common import *
from copy_config import CopyConfig


async def process_file(file, algo, digest):
    contents = await read_file(file, 'rb')
    hash = getattr(hashlib, algo)
    hash = hash()
    hash.update(contents)
    new = hash.hexdigest()
    assert new == digest, f'digests are not the same; new {new}, old {digest}'


async def main_async():
    # one *should* use argparse in such cases, but this one is trivially a single parameter, so why bother
    if not len(argv) >= 2:
        print('usage: python3 task2.py [config_file]')
        exit(1)
    fn = argv[1]
    cfg = await read_file(fn)
    lines = cfg.splitlines()
    tasks, files = [], []
    for line in lines:
        args = line.split(' ')
        tasks.append(process_file(*args))
        files.append(tuple(args))

    await execute_all(tasks, files)


if __name__ == '__main__':
    aio.run(main_async())
