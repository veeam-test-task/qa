from abc import ABC, abstractmethod, abstractproperty
from time import time
from os import remove
import psutil
from random import randbytes

from common import *


@dataclass
class SkipTest(BaseException):
    reason: str


class TestCase(ABC):
    @abstractmethod
    def prep(self):
        '''
      Either returns None or raises a SkipTest.
    '''
        pass

    @abstractmethod
    def run(self):
        '''
      Any exception raised indicates an error.
    '''
        pass

    @abstractmethod
    def clean_up(self):
        '''
      Guaranteed to not throw.
      It should be correct to call clean_up without calling prep/run at all.
    '''
        pass

    @abstractproperty
    def tc_id(self) -> int:
        pass

    @abstractproperty
    def name(self) -> str:
        pass

    def execute(self):
        try:
            self.prep()
            self.run()
            print(f'Test #{self.tc_id} ({self.name}): OK')
        except SkipTest as e:
            print(
                f'Test #{self.tc_id} ({self.name}): skipping test. Reason: \'{e.reason}\''
            )
        except BaseException as e:
            print(
                f'Test #{self.tc_id} ({self.name}): exception caught; \'{e}\'')
        finally:
            self.clean_up()


class ListOfFiles(TestCase):
    @property
    def name(self):
        return 'list of files'

    @property
    def tc_id(self):
        return 1

    def prep(self):
        if int(time()) % 2:
            raise SkipTest('time since epoch is odd')

    def run(self):
        print([str(x) for x in Path('.').iterdir()])

    def clean_up(self):
        pass


class RandomFile(TestCase):
    @property
    def name(self):
        return 'random file'

    @property
    def tc_id(self):
        return 2

    def prep(self):
        res = psutil.virtual_memory()
        if res.total < 2**30:
            raise SkipTest('need more RAM')

    def run(self):
        data = randbytes(2**20)
        with open('test', 'wb') as f:
            f.write(data)

    def clean_up(self):
        p = Path('test')
        if p.exists():
            remove(p)


def run():
    tests = [ListOfFiles(), ListOfFiles(), RandomFile(), RandomFile()]
    for t in tests:
        t.execute()


if __name__ == '__main__':
    run()
