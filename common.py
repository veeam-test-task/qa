import asyncio as aio
from dataclasses import dataclass, field
from itertools import chain, count, product
from pathlib import Path
from typing import List, Literal, Set, Tuple, Union

import aiofiles as aiof


async def read_file(f: Union[Path, str], mode='r'):
    '''
    Read contents of file f asynchronously.
  '''
    async with aiof.open(f, mode=mode) as f:
        return await f.read()


async def write_file(f: Union[Path, str], data: Union[str, bytes], mode='w'):
    '''
    Read contents of file f asynchronously.
  '''
    async with aiof.open(f, mode=mode) as f:
        await f.write(data)


async def execute_all(tasks, descs):
    '''
    Run all copy tasks asynchronously.
  '''
    for desc, result in zip(descs, await aio.gather(*tasks,
                                                    return_exceptions=True)):
        print(f'{desc}: ', end='')
        if result is None:
            print('OK')
        else:
            print(f'FAILED; {result}')


__all__ = [
    'product', 'count', 'chain', 'aio', 'dataclass', 'field', 'read_file',
    'write_file', 'Path', 'List', 'Literal', 'Set', 'Tuple', 'Union',
    'execute_all'
]
